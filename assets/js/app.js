// Animate on scroll
import AOS from 'aos'
import 'aos/dist/aos.css'

// jquery
import 'jquery'

// AnimateCSS
import '../css/modules/animate.css'

//replace me
import './modules/replaceme'

// Responsive Menu Javascript
import './parts/responsiveMenu'

// Separate Resize Function
import './parts/resize'

// Tooltip
import './parts/tooltip'

AOS.init()
