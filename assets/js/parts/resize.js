// Separate Items List
let home = document.getElementById('separate-home')
let contact = document.getElementById('separate-contact')
let single = document.getElementById('separate-single')
let circle = document.getElementById('circle')

function resize () {
    let x = document.body.clientWidth
    if (home !== null) { home.style['border-right-width'] = x + 'px' }
    if (contact !== null) { contact.style['border-left-width'] = x + 'px' }
    if (single !== null) { single.style['border-right-width'] = x + 'px' }

    if (circle !== null) { circle.style.height = circle.offsetWidth + 'px' }
}

resize()
window.addEventListener('resize', (e) => {
    resize()
})
