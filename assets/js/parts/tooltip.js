let elements = document.querySelectorAll('[tooltip]')

elements.forEach(function (e) {
    var text = e.getAttribute('tooltip')
    var tooltip = document.createElement('span')
    tooltip.classList.add('tooltip')
    tooltip.innerText = text
    e.appendChild(tooltip)
})
