let content = document.getElementById('mainNav')
let menu = document.getElementById('menu')
let sidebar = document.createElement('div')
sidebar.setAttribute('id', 'responsiveNav')
let overlay = document.getElementById('responsiveOverlay')
let button = document.getElementById('responsiveButton')
let activatedClass = 'responsive-activated'

menu.appendChild(sidebar)
sidebar.innerHTML = content.innerHTML

button.addEventListener('click', (e) => {
    e.preventDefault()
    sidebar.classList.add(activatedClass)
    overlay.classList.add(activatedClass)
})

overlay.addEventListener('click', (e) => {
    e.preventDefault()
    sidebar.classList.remove(activatedClass)
    overlay.classList.remove(activatedClass)
})
