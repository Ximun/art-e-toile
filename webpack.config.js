const path = require('path')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const ManifestPlugin = require('webpack-manifest-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const glob = require('glob')

const dev = process.env.NODE_ENV === "dev"

let cssLoaders = [
    { loader: 'css-loader', options: { importLoaders: 1,  minimize: !dev } }
]

if (!dev) {
    cssLoaders.push({
        loader: 'postcss-loader',
        options: {
            plugins: (loader) => [
                require('autoprefixer')({
                    browsers: ['last 2 versions', 'ie >= 7']
                }),
            ]
        }
    })
}

var config = {

    entry: {
        app: ['./assets/scss/app.scss', './assets/js/app.js']
    },
    watch: dev,
    output: {
        path: path.resolve('./dist/app'),
        filename: dev ? '[name].js' : '[name].js'
    },
    resolve: {
        alias: {
            '@scss': path.resolve('./assets/scss'),
            '@js': path.resolve('./assets/js')
        }
    },

    devtool: dev ? "cheap-module-eval-source-map" : false,

    devServer: {
        overlay: true
    },

    module: {
        rules: [
            {
              test: require.resolve('jquery'),
              use: [
                {
                  loader: 'expose-loader',
                  options: '$'
                },
                {
                  loader: 'expose-loader',
                  options: 'jQuery'
                }
              ]
            },

            // {
            //     enforce: 'pre',
            //     test: /\.js$/,
            //     exclude: /(node_modules|bower_components)/,
            //     use: ['eslint-loader']
            // },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader']
            },
            {
                test: /\.ejs$/,
                exclude: /(node_modules|bower_components)/,
                use: ['ejs-loader']
            },
            {
                test: /\.css$/,
                use: [
                    dev ? 'style-loader' : MiniCssExtractPlugin.loader,
                    ...cssLoaders,
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    dev ? 'style-loader' : MiniCssExtractPlugin.loader,
                    ...cssLoaders,
                    'sass-loader'
                ]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 8192,
                            name: 'img/[name].[hash:7].[ext]'
                        }
                    }
                ]
            },
            {
                loader: 'img-loader',
                options: {
                    enabled: !dev
                }
            }

        ]
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: dev ? '[name].css' : '[name].css',
            disable: dev
        })
    ]

}

if (!dev) {
    config.plugins.push(new UglifyJSPlugin({
        sourceMap: false
    }))
    config.plugins.push(new ManifestPlugin())
    config.plugins.push(new CleanWebpackPlugin(['dist/app'], {
        root: path.resolve('./'),
        verbose: true,
        dry: false
    }))
}

const files = glob.sync(process.cwd() + '/*.ejs')
files.forEach(file => {
    config.plugins.push(new HtmlWebpackPlugin({
        filename: path.join('..', '..', path.basename(file)).replace('.ejs', '.html'),
        template: file
    }))
})

module.exports = config
