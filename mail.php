<?php

Class Form {

  const DEST = 'simon@art-e-toile.com';
  const WEBSITE = 'art e-toile';

  private $headers = [
    "Content-Type" => "text/plain; charset=\"ISO-8859-1\"; DelSp=\"Yes\"; format=flowed",
    "Content-Disposition" => "inline",
    "Content-Transfer-Encoding" => "8bit",
    "MIME-Version" => "1.0"
  ];

  private $name;
  private $society;
  private $email;
  private $phone;
  private $describe;
  private $agree;
  private $hp;
    
  public function __construct($name, $society, $email, $phone, $describe, $agree) {
    $this->name = $this->clean($name);
    $this->society = $this->clean($society);
    $this->email = $this->clean($email);
    $this->phone = $this->clean($phone);
    $this->describe = $this->clean($describe);
    $this->agree = $this->clean($agree);
  }

  private function clean($field) {
    return htmlspecialchars(stripslashes(strip_tags($field)));
  }

  public static function is_set($post) {
    return ( (isset($post)) && (strlen(trim($post))) > 0 ) ? true : false;
  }

  public function send_main() {
    $dest = self::DEST;
    $subject = "[Site Web] " . $this->name;
    $content = "Nom de l'expéditeur : " . $this->name . "\r\n";
    $content .= "Société de l'expéditeur : " . $this->society . "\r\n";
    $content .= "Téléphone de l'expéditeur : " . $this->phone . "\r\n";
    $content .= "Consentement du traitement : " . $this->agree . "\r\n";
    $content .= $this->describe . "\r\n\r\n";

    $head = $this->headers;
    $head["From"] = $this->email;
    
    mail(self::DEST, $subject, utf8_decode($content), $head);
  }

  public function send_confirm() {
    $dest = $this->email;
    $subject = "[Art e-Toile] Merci " . $this->name . ", votre message a bien été envoyé";
    $content = "Message de confirmation
======================
Voici le message qui a été reçu:

Nom de l'expéditeur : {$this->name}
Société de l'expéditeur : {$this->society}
Téléphone de l'expéditeur : {$this->phone}
Consentement du traitement : {$this->agree}

{$this->describe}

Merci beaucoup, je vous répondrez dans les plus brefs délais.
Cordialement,
Simon";

    $head = $this->headers;
    $head["From"] = self::DEST;

    mail($this->email, $subject, utf8_decode($content), $head);
  }
}

$checks = [
  "name" => "Merci d'écrire un nom. <br/>",
  "email" => "Merci de renseigner votre adresse e-mail. <br/>",
  "describe" => "Merci d'écrire un message. <br/>",
  "agree" => "Vous devez accepter les conditions minimales de traitement de vos données personnelles. <br/>"
];

$stop = false;

foreach ($checks as $field => $message) {
  if (!Form::is_set($_POST[$field])) {
    echo $message;
    $stop = true;
    header("HTTP/1.1 501 No");
    } else if (isset($_POST["hp"]) && ($_POST["hp"] !== "")) {
    $stop = true;
  }
}

// Vérification mail ok
if (!filter_var(urldecode($_POST["email"]), FILTER_VALIDATE_EMAIL)) {
  echo "Votre adresse mail n'est pas valide.";
  $stop = true;
}

if ($stop == true ) {
  echo "<br/>";
  echo "Merci de revenir sur le <a href='contact.html'>formulaire de contact</a>.";
} else {
  $form = new Form($_POST["name"], $_POST["society"], $_POST["email"], $_POST["phone"], $_POST["describe"], $_POST["agree"]);
  try {
    $form->send_main();
    echo "e-mail envoyé :)";
    try {
      $form->send_confirm();
      echo "e-mail de confirmation envoyé";
    } catch (Exception $e) {
      echo "Envoi du mail de confirmation impossible.";
    }
  } catch (Exception $e) {
    echo "<br/>";
    echo "Envoi de l'email impossible.";
  }
}

?>
